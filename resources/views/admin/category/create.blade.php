@extends('admin.layouts.app')
@section('title') {{ 'Add-Category | '.env('APP_NAME') }} @endsection


{{-- @section('breadcrumbs')
    @include('admin-portal.layouts.partials.breadcrumbs',['current' => 'Add Article'])
    @endsection --}}


    @push('after-css')
    <style type="text/css">
     .error{color:red;}
   </style>
   @endpush


   @section('content')
   <div class="container-fluid">

     @if (Session::has('error'))
     <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span> </button>
        <h3 class="text-danger"><i class="fa fa-check-circle"></i> Error</h3>
        {{ Session::get('error') }}
      </div>

      @elseif(Session::has('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span> </button>
          <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3>
          {{ Session::get('success') }}
        </div>
        @endif

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- basic table -->
        <div class="row">
          <div class="col-md-12">
            <form method="post" id="add_cms" enctype="multipart/form-data" 
            action="{{url('/')}}/admin/submitcategory">
            {{csrf_field()}}


            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Add Category</h4>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="mt-2">Category Name</label>
                      <input class="form-control name"  
                      type="text" id="cms_title" maxlength="100" name="category_name" placeholder="Enter category name" >
                      <span class="invalid-feedback" id="cms_title_msg"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="mt-2">Category Image</label><br/>
                      <input type="file" id="brand_logo" required accept="image/x-png,image/gif,image/jpeg" name="category_image">
                      <br/><span class="error"  id="img_msg"></span>
                    </div>
                  </div>

                     <div class="col-md-12">
                    <div class="form-group">
                      <input type="submit" id="sbt_article"  value="Save" class="btn btn-primary btn-sm  mt-3">
                       <a  href="{{asset('/')}}/admin/category" class="btn btn-dark btn-sm mt-3 mr-2">Cancel</a>
                    </div>
                  </div>                    
                  </div>  
                </div>    
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>        
    @endsection
    @push('js')
    <script>

     
     

      $(document).ready(function()
      {       
       $("#sbt_article").on('click', function()
       {
        if($("#cms_title").val()=='')
        {
          $("#cms_title").addClass("is-invalid");
          $("#cms_title_msg").text('Enter name');
          setTimeout(function(){
            $("#cms_title").removeClass("is-invalid")
            $("#cms_title_msg").text('');
          }, 3000);
          return false;
        }   

        if(document.getElementById("brand_logo").files.length == 0 )
        {
            $("#img_msg").text('Please select logo file');
             setTimeout(function(){
            $("#img_msg").text('');
          }, 3000);
            return false;
        }   




       

       /* $("#add_cms").submit();
          return true;    */
      });
     });



   </script>

   @endpush