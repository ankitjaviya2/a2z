@extends('admin.layouts.app')
@section('title') {{ 'Manage-Category | '.env('APP_NAME') }} @endsection


{{-- @section('breadcrumbs')
    @include('admin-portal.layouts.partials.breadcrumbs',['current' => 'Add Article'])
    @endsection --}}


    @push('after-css')
    <style type="text/css">
     .error{color:red;}
     #example_filter{
      float: right!important;
    }
  </style>
  @endpush


  @section('content')
  <div class="container-fluid">

   @if (Session::has('error'))
   <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span> </button>
      <h3 class="text-danger"><i class="fa fa-check-circle"></i> Error</h3>
      {{ Session::get('error') }}
    </div>

    @elseif(Session::has('success'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span> </button>
        <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3>
        {{ Session::get('success') }}
      </div>
      @endif

      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- basic table -->
      <div class="row">
        <div class="col-md-12">
          <form method="post" id="add_cms" enctype="multipart/form-data" 
          action="{{url('/')}}/admin/submit_brand">
          {{csrf_field()}}


          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-10">
                  <h4 class="card-title">Manage Category</h4>
                </div>
                <div class="col-md-2">
                 <a href="{{url('/')}}/admin/createcategory" class="btn btn-block btn-primary btn-sm">Add New</a>             
               </div>
             </div>

             <div class="row">
              <div class="col-md-12 mt-3">
                <table id="example" class="table table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>Category Name</th>
                      <th>Image</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($data)
                    @foreach($data as $dtl)
                    <tr>
                      <td>{{$dtl->category_name}}</td>
                      <td>
                        @if(!empty($dtl->category_image))
                          <img src="{{asset('/')}}/uploads/category/{{$dtl->category_image}}" class="Thumbnail" height="50px" width="50px">
                        @else
                           NA
                        @endif
                        
                      </td>
                      <td>
                          @if($dtl->is_active=='Y')
                      <a href="{{url('/').'/admin/categorystatus/'.base64_encode($dtl->id)}}">
                        <span class="activebtn">
                           <i class="fa fa-unlock" title="Active"></i> 
                        </span></a>
                          
                  @elseif($dtl->is_active=='N')
                      <a href="{{url('/').'/admin/categorystatus/'.base64_encode($dtl->id)}}">
                        <span class="inactivebtn"> <i class="fa fa-lock" title="Inactive"></i></span></a>
                  @endif

                      </td>
                      <td>
                          <a type="button" href="{{url('/').'/admin/editcategory/'.base64_encode($dtl->id)}}" name="edit"  class="mr-1" title="EDIT"><i class="fas fa-edit text-primary"></i></a>

                        <a href="{{url('/').'/admin/deletecategory/'.base64_encode($dtl->id)}}" class="mr-1 delete"  title="DELETE"><i class="fa fa-times text-danger"></i></a>

                      </td>
                    </tr>

                    @endforeach
                    @endif
                  
                  </tbody>

                </table>
              </div>



            </div>  
          </div>    
        </div>
      </div>
    </form>
  </div>
</div>
</div>        
@endsection
@push('js')
<script src="{{asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script> 
<script>
  $(document).ready(function() {
    $('#example').DataTable();
  } );

   $(document).on('click', '.delete', function (e) {
        if (confirm('Are you sure want to delete?')) 
           return true;
         else 
            return false;
    });


</script>

@endpush