@extends('admin.layouts.app')
@section('title') {{ 'Dashboard | '.env('APP_NAME') }} @endsection
@push('after-css')

<style type="text/css">
    
</style>

@endpush
@section('content')
    <div class="container-fluid">

          <h4 class="card-title">Dashboard</h4>
          <div class="row">
            
                    <div class="col-md-6 col-lg-3">
                        <a href="{{url('/')}}/admin/category">
                            <div class="card oh" style="background-color: #76cef0;">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div>
                                         <span style="color:white!important" class="text-dark font-medium op-7 d-block mb-2">
                                            <i class="fas fa-list-ul"></i> Category</span>
                                            <h3 style="color:white!important" class="mb-0">  {{$total_cat}} </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </a>
                    </div>
                  
                      <div class="col-md-6 col-lg-3">
                        <a href="{{url('/')}}/admin/product">
                            <div class="card oh" style="background-color: #76cef0;">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <div>
                                         <span style="color:white!important" class="text-dark font-medium op-7 d-block mb-2">
                                            <i class="fas fa-list-ul"></i> Products </span>
                                            <h3 style="color:white!important" class="mb-0">  {{$total_prd}} </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </a>
                    </div>
                </div>
        <div class="row">
                   {{--  <div class="col-md-6 col-lg-3">
                        <div class="card oh">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                     
                                       
                                    </div>
                                    
                                </div>
                            </div>
                         
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card oh">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                      
                                        
                                    </div>
                                  
                                </div>
                            </div>
                         
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card oh">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                    
                                       
                                    </div>
                                 
                                </div>
                            </div>
                          
                        </div>
                    </div> --}}
                  {{--   <div class="col-md-6 col-lg-3">
                        <div class="card oh">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <span class="text-dark font-medium op-7 d-block mb-2">New Customers</span>
                                        <h3 class="mb-0">180</h3>
                                    </div>
                                 
                                </div>
                            </div>
                         
                        </div>
                    </div> --}}
                </div>


 

       
        <!-- ============================================================== -->
        <!-- Table -->
        <!-- ============================================================== -->
    </div>
@endsection
@push('js')
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="{{asset('assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--c3 charts -->
    <script src="{{asset('assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{asset('assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{asset('dist/js/pages/dashboards/dashboard1.js')}}"></script>


    <!-- Internal JS -->

@endpush