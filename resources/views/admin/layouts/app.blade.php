<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <title>@yield('title','Mendy Admin Template')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @stack('before-css')

    <!-- Custom CSS -->
    <link href="{{asset('assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
    <link href="{{asset('dist/css/style.css')}}" rel="stylesheet">
   <!--  <link href="{{asset('dist/css/custom.css')}}" rel="stylesheet"> -->
    <link href="{{asset('dist/css/admin-portal.css')}}" rel="stylesheet">
    <!-- <link href="{{asset('assets/toastr.css')}}" rel="stylesheet"> -->
    
    
    @stack('after-css')
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper"  class="@if(session()->get('theme-layout') == 'fix-header')
       boxed-layout @endif">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    @include('admin.layouts.partials.topbar')
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    @include('admin.layouts.partials.left-sidebar')
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper" style="padding-top:0px !important;">

    @include('admin.layouts.partials.notifications')

    <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
    @yield('breadcrumbs')
    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
    @yield('content')
    <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
    @include('admin.layouts.partials.footer')
    <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>

<div id="responsiveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static"> </div>
<input type="hidden" data-toggle="modal" data-target="#responsiveModal" id="commonModel" />

<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
        


@if(auth()->check() && auth()->user()->user_type=='A')
    
{{--     @php    
        dd(auth()->user()->user_type);
    @endphp
 --}}

    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    @include('admin.layouts.partials.customiser-panel')
@endif
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- apps -->
<script src="{{asset('dist/js/app.min.js')}}"></script>

@if(session()->get('theme-layout') == 'fix-header')

    <script src="{{asset('dist/js/app.init.horizontal.js')}}"></script>
   <!---=========== Use Above JS for Horizontal Layout ==========--->

@elseif(session()->get('theme-layout') == 'mini-sidebar')

    <script src="{{asset('dist/js/app.init.mini-sidebar.js')}}"></script>
    <!---========= Use Above JS for Mini Sidebar Layout =========--->

@else

    <script src="{{asset('dist/js/app.init.js')}}"></script>
    <!---========= Use Above JS Default Layout ==========--->

@endif

<script src="{{asset('dist/js/app-style-switcher.js')}}"></script>
<!-- slims-crollbar scrollbar JavaScript -->
<script src="{{asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>

<!-- slim-scrollbar scrollbar JavaScript -->

<!--Wave Effects -->
<!--Wave Effects -->
<script src="{{asset('dist/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('dist/js/sidebarmenu.js')}}"></script>
<script src="{{asset('dist/js/feather.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('dist/js/custom.min.js')}}"></script>
<!-- <script src="{{asset('assets/toastr.js')}}"></script> -->
<script>
    var base_url = "{{ url('/') }}";
</script>

@stack('js')
</body>
</html>