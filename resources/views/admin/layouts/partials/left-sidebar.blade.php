<aside class="left-sidebar" style="background-color: #2F183E!important;box-shadow: 2px 0 2px 0px #888;">
  <!-- Sidebar scroll-->
  <div class="scroll-sidebar">
    <!-- Sidebar navigation-->

    <nav class="sidebar-nav">
      <ul id="sidebarnav">

        <li class="sidebar-item mt-4">
          <a class="sidebar-link" href="{{url('/')}}/admin/dashboard"
          aria-expanded="false">
          <i data-feather="home" class="mr-2"></i>
          <span class="hide-menu">Dashboard</span>
        </a>
      </li>

        <li class="sidebar-item mt-1">
          <a class="sidebar-link" href="{{url('/')}}/admin/category"
          aria-expanded="false">
          <i data-feather="list" class="mr-2"></i>
          <span class="hide-menu">Manage Category</span>
        </a>
      </li>

       <li class="sidebar-item mt-1">
          <a class="sidebar-link" href="{{url('/')}}/admin/product"
          aria-expanded="false">
          <i data-feather="list" class="mr-2"></i>
          <span class="hide-menu">Manage Products</span>
        </a>
      </li>

     

   {{--  <li class="sidebar-item">
      <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i data-feather="users" class="mr-2"></i>
        <span class="hide-menu">Navigators </span>
      </a>
      <ul aria-expanded="false" class="collapse first-level">
        <li class="sidebar-item">
          <a class="sidebar-link" href="{{url('/')}}/admin/navigator"
          aria-expanded="false">
          <i data-feather="user" class="mr-2"></i>
          <span class="hide-menu">Manage Navigators</span>
        </a>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link" href="{{url('/')}}/admin/navigatorgroup"
        aria-expanded="false">
        <i data-feather="users" class="mr-2"></i>
        <span class="hide-menu">Navigator Groups</span>
      </a>
    </li>

  </ul>
</li> --}}



<hr/>




{{-- <li class="sidebar-item">
    <a class="sidebar-link" href="{{url('/')}}/admin/sra"
    aria-expanded="false">
    <i data-feather="file" class="mr-2"></i>
    <span class="hide-menu">SRA</span>
</a>
</li>  --}}



{{-- <li class="sidebar-item">
    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
      <i class="far fa-list-alt mr-2"></i>
      <span class="hide-menu"> CRA </span>
  </a>
  <ul aria-expanded="false" class="collapse first-level">
    <li class="sidebar-item">
        <a href="{{url('/')}}/admin/manage_cra" class="sidebar-link">
          <i data-feather="file" class="mr-2"></i>
          <span class="hide-menu"> Manage CRA </span>
      </a>
    </li> --}}
{{--   <li class="sidebar-item">
    <a href="{{url('/')}}/admin/crareport" class="sidebar-link">
      <i data-feather="file" class="mr-2"></i>
      <span class="hide-menu"> CRA Reports </span>
  </a>
</li> --}}

{{-- </ul>
</li> --}}

{{-- 
<li class="sidebar-item">
  <a class="sidebar-link" href="{{url('/')}}/admin/manage_cra"
  aria-expanded="false">
  <i data-feather="user" class="mr-2"></i>
  <span class="hide-menu">Manage CRA</span>
</a>
</li> 
 --}}

{{-- <li class="sidebar-item">
    <a class="sidebar-link" href="{{url('/')}}/admin/screeners"
    aria-expanded="false">
    <i data-feather="user" class="mr-2"></i>
    <span class="hide-menu">Clinical Parameter</span>
</a>
</li> 
--}}
{{-- <li class="sidebar-item">
    <a class="sidebar-link" href="{{url('/')}}/admin/classes"
    aria-expanded="false">
    <i data-feather="file" class="mr-2"></i>
    <span class="hide-menu">Requested Classes</span>
</a>
</li>
--}}
{{-- <li class="sidebar-item">
  <a class="sidebar-link" href="{{url('/')}}/admin/cms"
  aria-expanded="false">
  <i data-feather="file" class="mr-2"></i>
  <span class="hide-menu">Manage CMS</span>
</a>
</li> 

<li class="sidebar-item">
  <a class="sidebar-link" href="{{url('/')}}/admin/article"
  aria-expanded="false">
  <i data-feather="file" class="mr-2"></i>
  <span class="hide-menu">Manage Articles</span>
</a>
</li> --}}



<li class="sidebar-item">
  <a class="sidebar-link" href="{{url('/logout')}}"
  aria-expanded="false">
  <i data-feather="log-out" class="mr-2"></i>
  <span class="hide-menu">Logout</span>
</a>
</li>    


</ul>
</nav>


</div>
</aside>