@extends('admin.layouts.app')
@section('title') {{ 'Edit-Product | '.env('APP_NAME') }} @endsection


{{-- @section('breadcrumbs')
    @include('admin-portal.layouts.partials.breadcrumbs',['current' => 'Add Article'])
    @endsection --}}


    @push('after-css')
    <style type="text/css">
     .error{color:red;}
     .img-thumbnail
     {
       height: 100px;
       width: 100px;
       margin-left: 5px;
       margin-right: 5px;
       margin-top: 10px;
     }

     
   </style>
   @endpush


   @section('content')
   <div class="container-fluid">

     @if (Session::has('error'))
     <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span> </button>
        <h3 class="text-danger"><i class="fa fa-check-circle"></i> Error</h3>
        {{ Session::get('error') }}
      </div>

      @elseif(Session::has('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span> </button>
          <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3>
          {{ Session::get('success') }}
        </div>
        @endif

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- basic table -->
        <div class="row">
          <div class="col-md-12">
            <form method="post" id="add_cms" enctype="multipart/form-data" 
            action="{{url('/')}}/admin/updateproduct">
            {{csrf_field()}}

            <input type="hidden" name="updateid" value="{{$data['id']}}">

            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Edit Product</h4>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Name</label>
                      <input class="form-control name"   value="{{$data['product_name']}}" 
                      type="text" id="cms_title" maxlength="100" name="product_name" 
                      placeholder="Enter product name" >
                      <span class="invalid-feedback" id="cms_title_msg"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Category Name</label>
                      <select name="category_name" class="form-control">
                        <option>- Select Category-</option>
                        @if($catlist)
                        @foreach($catlist as $ct)
                        <option {{$data['category_id']==$ct->id ? 'selected="selected"' : ''}} 
                          value="{{$ct->id}}">{{$ct->category_name}}</option>
                        @endforeach
                        @endif
                      </select>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Code</label><br/>
                      <input type="text" id="prd_code" class="form-control"  name="product_code" placeholder="Enter Product Code"  value="{{$data['product_code']}}" >
                      <span class="error"  id="prd_code_msg"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Size</label><br/>
                       <div class="form-group">
                      <label class="mt-2">Product Size</label><br/>
                      <input type="checkbox" class="ml-2" {{strpos($data['product_size'],"S") !== false ?  'checked="checked"' : '' }}     name="product_size[]" value="S"> S - Small
                      <input type="checkbox" class="ml-2" name="product_size[]" {{strpos($data['product_size'],"M") !== false ?  'checked="checked"' : '' }}    value="M"> M - Medium
                      <input type="checkbox" class="ml-2" name="product_size[]" {{strpos($data['product_size'],"L") !== false ?  'checked="checked"' : '' }}    value="L"> L - Large
                      <input type="checkbox" class="ml-2" name="product_size[]" {{strpos($data['product_size'],"XL") !== false ?  'checked="checked"' : '' }}    value="XL"> XL - Extra Large
                    </div>
                      
                      <br/><span class="error"  id="img_msg"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Price</label><br/>
                      <input type="text" id="prd_price" maxlength="10" onkeypress="return isNumber(event)" class="form-control" required value="{{$data['product_price']}}"  name="product_price" placeholder="Enter Product Price">
                      <span class="error"  id="prd_price_msg"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Product Description</label><br/>
                      <textarea id="add_content"  name="content" class="form-control"><?=$data['product_descp']?></textarea>
                      <span class="error" id="descp_error_msg"></span>
                    </div>
                  </div>



                  <div class="col-md-12">
                  <h4> Existing Images & Colors </h4>

                    <div class="form-group">
                      <div class="row">
                          @foreach($data['prd_colors'] as $dtl)
                          <div class="col-md-2">
                            <div class="thumbnail">
                                <img class="img-thumbnail" src="{{ url('/')}}/uploads/product/{{$dtl['product_image']}}" alt="" style="width:100%">
                                <div class="caption">
                                  <p style="color: black;text-align: center;padding-top: 5px">{{$dtl['product_color']}} <a  href="{{url('/')}}/admin/deletproductimage/{{base64_encode($dtl->id)}}" title="Delte Image" class="btn  delete btn-xs ml-2" href=""> 
                                    <i class="fa fa-close"> </i>
                                   </a>
                                  </p>
                                   
                                </div>
                            </div>
                          </div>
                          @endforeach
                         
                          
                        </div>
                  </div>
                </div>

                 <div class="col-md-12">
                  <h4> Product Images & Colors </h4>

                    <div class="form-group">
                     <table class="form-table table table-bordered dataTable no-footer" id="customFields">
                      <tr valign="top">
                        <th scope="row"><label for="customFieldName">Image Color & Image</label></th>
                        <td>
                          
                          <input type="text" class="code" id="customFieldName" name="color_name[]"  placeholder="Color Name" /> &nbsp;
                          <input type="file" multiple id="gallery-photo-add"  class="code gallery-photo-add" id="customFieldValue" name="prd_image[]"   accept="image/x-png,image/gif,image/jpeg"  /> &nbsp; 
                          <div class="gallery"></div>


                        </td>
                        <td>
                          <a href="javascript:void(0);" class="btn btn-info btn-sm" id="addCF">Add</a>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>


                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="submit" id="sbt_article"  value="Save" class="btn btn-primary btn-sm  mt-3">
                      <a  href="{{asset('/')}}/admin/product" class="btn btn-dark btn-sm mt-3 mr-2">Cancel</a>
                    </div>
                  </div>                    
                </div>  
              </div>    
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>        
  @endsection
  @push('js')
  <script src="{{asset('assets/tinymce/tinymce.min.js')}}"></script>
  <script>

$(document).on('click', '.delete', function (e) {
        if (confirm('Are you sure want to delete?')) 
           return true;
         else 
            return false;
    });

    $(document).ready(function(){
      $("#addCF").click(function(){
        $("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Image Color & Image</label></th><td><input type="text" class="code" id="customFieldName" name="color_name[]"  placeholder="Input Name" /> &nbsp; <input type="file" multiple id="gallery-photo-add"  class="code gallery-photo-add" id="customFieldValue" name="prd_image[]"   accept="image/x-png,image/gif,image/jpeg"  /> &nbsp; <div class="gallery"></div> </td> <td>  <a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Remove</a></td></tr>');
        $(".remCF").on('click',function(){
          $(this).parent().parent().remove();
        });
      });
    });

    $(document).ready(function()
    {
      var tbody = $('#myTable').children('tbody');

//Then if no tbody just select your table 
var table = tbody.length ? tbody : $('#myTable');


$('button').click(function(){
    //Add row
    table.append('<tr><td><input type="text"/></td></tr><tr><td><input type="file" multiple/></td><div class="gallery"></div></tr>');
  })

console.log('test3');
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
      console.log('test');
      if (input.files) {
        var filesAmount = input.files.length;

        for (i = 0; i < filesAmount; i++) {
          var reader = new FileReader();

          reader.onload = function(event) {
            $($.parseHTML('<img class="img-thumbnail">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
          }

          reader.readAsDataURL(input.files[i]);
        }
      }

    };

    $(document).on('change','.gallery-photo-add', function ()
    { 
      var arr = [];
      //var numFiles = $("input", this)[0].files.length;
      var numFiles1 = $(this).parents('td').find('.gallery-photo-add')[0].files.length;
      var x = $(this).parents('td').find('div.gallery');
      imagesPreview(this, x);

      var y = $(this).parents('td').find('.code').val();
      console.log(y);

      for (var i = 0; i < numFiles1; i++) 
      {
        $(this).parents('td').append("<input type='hidden' name='loop[]' value="+y+"  />");
       arr[i] = y; 
      }      
      //console.log('test2');
    });


  });

    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }


    tinymce.init({ 
      selector: '#add_content',
      plugins: "textcolor, colorpicker,table,lists,link,autoresize,pagebreak,insertdatetime",
      toolbar: "forecolor backcolor,fontsizeselect,table,numlist bullist,link,pagebreak,insertdatetime",
      fontsize_formats: "8px 9px 10px 11px 12px 13px 14px 15px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px",
      image_advtab: true,

    // without images_upload_url set, Upload tab won't show up
    images_upload_url: '/admin/saveImage',
    
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
      var xhr, formData;
      
      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open('POST', "{{ url('/admin/saveImage')}}" );
      xhr.setRequestHeader(  'X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
      
      xhr.onload = function() {
        var json;
        
        if (xhr.status != 200) {
          failure('HTTP Error: ' + xhr.status);
          return;
        }
        
        json = JSON.parse(xhr.responseText);
        
        if (!json || typeof json.location != 'string') {
          failure('Invalid JSON: ' + xhr.responseText);
          return;
        }

        console.log(json.location);

        success(json.location);
      };
      
      formData = new FormData();
      formData.append('file', blobInfo.blob(), blobInfo.filename());
      
      xhr.send(formData);
    },
  });


    $(document).ready(function()
    {       
     $("#sbt_article").on('click', function()
     {
      if($("#cms_title").val()=='')
      {
        $("#cms_title").addClass("is-invalid");
        $("#cms_title_msg").text('Enter product name');
        setTimeout(function(){
          $("#cms_title").removeClass("is-invalid")
          $("#cms_title_msg").text('');
        }, 3000);
        return false;
      }   

      if($("#prd_code").val()=='')
      {
        $("#prd_code").addClass("is-invalid");
        $("#prd_code_msg").text('Enter product code');
        setTimeout(function(){
          $("#prd_code").removeClass("is-invalid")
          $("#prd_code_msg").text('');
        }, 3000);
        return false;
      }   

      if($("#prd_price").val()=='')
      {
        $("#prd_price").addClass("is-invalid");
        $("#prd_price_msg").text('Enter product price');
        setTimeout(function(){
          $("#prd_price").removeClass("is-invalid")
          $("#prd_price_msg").text('');
        }, 3000);
        return false;
      }   
      var editorContent = tinyMCE.get('add_content').getContent();
      if (editorContent == '')
      { 
        $("#descp_error_msg").text('Please enter description');
        setTimeout(function(){
          $("#descp_error_msg").text('');
        }, 3000);
        return false;
      }



       /* $("#add_cms").submit();
       return true;    */
     });
   });



 </script>

 @endpush