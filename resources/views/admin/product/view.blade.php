@extends('admin.layouts.app')
@section('title') {{ 'View-Product | '.env('APP_NAME') }} @endsection


{{-- @section('breadcrumbs')
    @include('admin-portal.layouts.partials.breadcrumbs',['current' => 'Add Article'])
    @endsection --}}


    @push('after-css')
    <style type="text/css">
     .error{color:red;}
   </style>
   @endpush


   @section('content')
   <div class="container-fluid">

     @if (Session::has('error'))
     <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span> </button>
        <h3 class="text-danger"><i class="fa fa-check-circle"></i> Error</h3>
        {{ Session::get('error') }}
      </div>

      @elseif(Session::has('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span> </button>
          <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3>
          {{ Session::get('success') }}
        </div>
        @endif

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- basic table -->
        <div class="row">
          <div class="col-md-12">
            <form method="post" id="add_cms" enctype="multipart/form-data" 
            action="{{url('/')}}/admin/submit_helment">
            {{csrf_field()}}


            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Product Information</h4>
                <div class="row">

                  <div class="col-md-12">
                    <div class="col-lg-12">
                      <div class="card">
                        <div class="card-body">

                          <div class="row">     
                           <h3 class="card-title">
                           {{!empty($data['product_name']) ? $data['product_name'] : 'NA' }}</h3>  <hr/>
                           <div class="col-lg-12 col-md-12 col-sm-12">
                            <h4 class="box-title m-t-10">Product description</h4>
                            <?= $data['product_descp'] ?>
                            <h3 class="m-t-5"> Price:  <small class="text-success">
                              {{$data['product_price']!="" ? "₹ ".$data['product_price'] : ''  }}
                            </small></h3>


                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12">
                            <h3 class="box-title m-t-10">General Info</h3>
                            <div class="table-responsive">
                              <table class="table">
                                <tbody>
                                  <tr>
                                    <td width="390">Product Code</td>
                                    <td> 
                                      {{$data['product_code']!="" ? $data['product_code'] : ''  }}
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Product Size</td>
                                    <td>  {{$data['product_size']!="" ? $data['product_size'] : ''  }} </td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <h3>Product Image</h3>
                        <div class="row">
                          @foreach($data['prd_colors'] as $dtl)
                          <div class="col-md-2">
                            <div class="thumbnail">
                                <img class="img-thumbnail" src="{{ url('/')}}/uploads/product/{{$dtl['product_image']}}" alt="" style="width:100%">
                                <div class="caption">
                                  <p style="color: black;text-align: center;padding-top: 5px">{{$dtl['product_color']}}</p> 
                                
                                </div>
                            
                            </div>
                          </div>
                          @endforeach
                         
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                 <a  href="{{asset('/')}}admin/product" class="btn btn-sm btn-dark mr-2">Cancel</a>
               </div>  
             </div>    
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>        
 @endsection
