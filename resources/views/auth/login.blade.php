@extends('auth.layouts.main')
@section('title') {{ 'A2Z | PORTAL' }} @endsection

@push('before-css')
<style type="text/css">
    .loginbtn{background-color:#2F183E!important; border-color: #2F183E!important }
</style>
@endpush



@section('content')
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" 
    style="background-color: #2F183E!important">
        <div class="auth-box">

    
            @if (count($errors) > 0)
                              <div class="alert alert-danger alert-rounded"> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span> </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
             @endif

             @if(Session::has('error'))
           <div class="alert alert-danger alert-rounded"> 
                 {{Session::get('error') }}
             <button type="button" class="close" style="margin-top: 0px !important;padding: 0px !important;" data-dismiss="alert" aria-hidden="true">&times;</button>
          </div>
        @endif
        
                
    
            <div id="loginform">
                <div class="logo">
                   <span class="db">{{-- <img src="{{url('assets/images/White Background.png')}}"
                    width="100px;" alt="logo" /> --}}</span>
                    <h5 class="font-medium m-b-20 mt-3">A2Z | Admin </h5>
                </div>
                <!-- Form -->
                <div class="row">
                    <div class="col-12">
                        <form class="form-horizontal mt-3" method="post" action="{{ route('userlogin') }}" id="loginform">
                           @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-lg" placeholder="Username"aria-label="Username"  name="email"   
                                value="{{@$_COOKIE['member_login']}}"  
                                       aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                                </div>
                                <input type="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password" 
                                value="{{@$_COOKIE['member_password']}}"  name="password" aria-describedby="basic-addon1">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember_me" class="custom-control-input" id="remember_me" 
                                        value="1"
                                        <?php if(isset($_COOKIE['member_login'])){ ?>
                                        checked <?php } ?>  />
                                        <label class="custom-control-label" for="remember_me">Remember me</label>
                                     
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="col-xs-12 p-b-20">
                                    <button class="loginbtn btn btn-block btn-lg btn-primary  text-white"  type="submit">Log
                                        In</button>
                                </div>
                            </div>
                        {{--     <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                    <div class="social">
                                        <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"
                                           title="" data-original-title="Login with Facebook"> <i aria-hidden="true"
                                                                                                  class="fab  fa-facebook"></i> </a>
                                        <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"
                                           title="" data-original-title="Login with Google"> <i aria-hidden="true"
                                                                                                class="fab  fa-google-plus"></i> </a>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group m-b-0 m-t-10">
                                <div class="col-sm-12 text-center" >
                                      <a href="javascript:void(0)" id="to-recover" class="text-dark"><i
                                                    class="fa fa-lock m-r-5"></i> Forgot  Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="recoverform">
                <div class="logo">
                    <span class="db">{{-- <img src="{{asset('assets/images/white.png')}}" alt="logo" /> --}}</span>
                    <h5 class="font-medium m-b-20">Recover Password</h5>
                    <span>Enter your Email and instructions will be sent to you!</span>
                </div>
                <div class="row m-t-20">
                    <!-- Form -->
                    <form class="col-12" action="{{asset('/')}}">
                        <!-- email -->
                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control form-control-lg" type="email" required=""
                                 placeholder="Email">
                            </div>
                        </div>
                        <!-- pwd -->
                        <div class="row m-t-20">
                            <div class="col-12">
                                <button class="loginbtn btn btn-block btn-lg  text-white" type="submit"
                                        name="action">Reset</button>

                             <a href="javascript:void(0)" id="to-login" class="loginbtn btn btn-block btn-lg btn-primary  text-white"  type="submit">LogIn</a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
@endsection
@push('js')
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();
        // ==============================================================
        // Login and Recover Password
        // ==============================================================
        $('#to-recover').on("click", function () {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $('#to-login').on("click", function () {
            $("#loginform").fadeIn();
            $("#recoverform").slideUp();
        });

      
    </script>
@endpush
