@extends('auth.layouts.main')
@section('title') {{ 'THRIVE | PORTAL' }} @endsection

@push('before-css')
<style type="text/css">
    .loginbtn{background-color: #33096e!important; border: 1px solid #33096e!important }
</style>
@endpush

@section('content')

    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" 
    style="background-color: #33096e!important">
        <div class="auth-box">

    
            @if (count($errors) > 0)
                              <div class="alert alert-danger alert-rounded"> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span> </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
             @endif



             @if(Session::has('error'))
           <div class="alert alert-danger alert-rounded"> 
                 {{Session::get('error') }}
             <button type="button" class="close" style="margin-top: 0px !important;padding: 0px !important;" data-dismiss="alert" aria-hidden="true">&times;</button>
          </div>
        @endif

          @if(Session::has('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span> </button>
          <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3>
          {{ Session::get('success') }}
        </div>
        @endif
        
                
    
            <div id="loginform">
                <div class="logo">
                   <span class="db"><img src="{{asset('assets/images/White Background.png')}}"
                    width="100px;" alt="logo" /></span>
                     <h5 class="font-medium m-b-20">Recover Password</h5>
                     <span>Enter your Email and instructions will be sent to you!</span>
                </div>
                <div class="row m-t-20">
                    <!-- Form -->
                    <form class="col-12" method="post" action="{{url('/')}}/forget_password">
                        {{csrf_field()}}
                        <!-- email -->
                        <div class="form-group row">
                            <div class="col-12">

                                <input type="email" class="form-control form-control-lg"
                                value="{{old('reset_email')}}"
                                 required name="reset_email" id="email"  placeholder="Email">


                            </div>
                        </div>
                        <!-- pwd -->
                        <div class="row m-t-20">
                            <div class="col-12">
                                <button class="loginbtn btn btn-block btn-lg  text-white" type="submit"
                                        name="action">Reset</button>

                             <a href="{{url('/')}}" id="to-login" class="loginbtn btn btn-block btn-lg btn-primary  text-white"  type="submit">Log In</a>

                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
@endsection
@push('js')

@endpush
