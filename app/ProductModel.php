<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'product_name', 'product_code','product_descp','product_size','product_price','is_active','created_at'];
    public $timestamps = false;

    public function prd_colors()
    {
    	return $this->hasMany('App\ProductColorModel','product_id','id');
    }

}
