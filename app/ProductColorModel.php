<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColorModel extends Model
{
    protected $table = 'product_colors';
    protected $primaryKey = 'id';
    protected $fillable = ['product_id', 'product_image', 'product_color'];
    public $timestamps = false;
}
