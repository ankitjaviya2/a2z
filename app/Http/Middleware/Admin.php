<?php

namespace App\Http\Middleware;
use Closure;
use Auth;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (!Auth::check()) 
        {   
           return redirect('/login')->with('error',"You don't have admin access.");
        }

        if (Auth::user()) 
        {
            // return redirect()->route('admin');
            return $next($request);
        }
        else
        {
            return redirect('/login')->with('error',"You don't have admin access.");
        }
    }
}
