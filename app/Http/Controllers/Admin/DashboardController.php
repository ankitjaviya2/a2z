<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryModel;
use App\ProductModel;

class DashboardController extends Controller
{
    public function __construct()
    {

	}
	public function index()
	{	
		$prod = ProductModel::count();
		$cat = CategoryModel::count();

		$total_cat = $cat<10 ? "0".$cat : $cat; 
		$total_prd = $prod<10 ? "0".$prod : $prod; 
		return view('admin.dashboard',compact('total_cat','total_prd'));
	}
}
