<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryModel;
use App\ProductModel;
use App\ProductColorModel;
use Flash;
use Session;
use Storage;
use DB;
use DateTime;
use Tooleks\Php\AvgColorPicker\Gd\AvgColorPicker;


class ProductController extends Controller
{
    public function __construct(ProductModel $productdtl, ProductColorModel $prdColors)
    {
        $this->product = $productdtl;
        $this->prd_color = $prdColors;
        $this->product_path         = public_path() . config('app.project.product_path');
        $this->product_public_path = url('/') ."/public/".config('app.project.product_path');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $data =  $this->product->orderBy('id','DESC')->get();
       return view('admin.product.index',compact('data'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createproduct()
    {
        $catlist = CategoryModel::where('is_active','Y')->orderBy('category_name','asc')->get();
        return view('admin.product.create',compact('catlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $ProductName = $request->product_name;
        $catName = $request->category_name;
        $prd_code = $request->product_code;
        $prd_price = $request->product_price;


        if($request->has('product_size'))
           $product_size =  implode(",",$request->product_size);
       else
           $product_size =  null; 

       $descp = $request->content;

       $saveData = [];

       $saveData['category_id'] = $catName;
       $saveData['product_name'] = $ProductName;
       $saveData['product_code'] = $prd_code;
       $saveData['product_descp'] = $descp;
       $saveData['product_size'] = $product_size;
       $saveData['product_price'] = $prd_price;
       $saveData['is_active'] =  'Y';

       $isSave = $this->product->create($saveData);
       $last_id = $isSave->id;

       if($request->hasFile('prd_image'))
       {         
        foreach($data['prd_image'] as $key1 => $image)
        {       
            $images_name = $image->getClientOriginalName();
            $image->move($this->product_path,$images_name);  
            $arr_data = [];
            $arr_data['product_id'] = $last_id;
            $arr_data['product_image'] = $images_name;
            foreach ($data['loop'] as $key2 => $val3) 
            {
                if($key1==$key2)
                {   
                  $arr_data['product_color'] = $val3; 
              }
          }
          $this->prd_color->create($arr_data);            
      }
  }

  Session::flash('success','Data has been saved successfully.');
  return redirect('admin/product');
}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id!=null)
        {
            $data =  $this->product->with('prd_colors')->where('id',base64_decode($id))->first();
            if(!empty($data))
            {
                $data = $data->toArray();
            }
            return view('admin.product.view',compact('data'));
        }
        else
        {
            return redirect(config('app.project.login_slug'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if($id!=null)
        {
         $catlist = CategoryModel::where('is_active','Y')->orderBy('category_name','asc')->get();

         $data =  $this->product->with('prd_colors')->where('id',base64_decode($id))->first();
         return view('admin.product.edit',compact('data','catlist'));
     }
     else
     {
        return redirect(config('app.project.login_slug'));
    }
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $updateid = $request->updateid;
        $ProductName = $request->product_name;
        $catName = $request->category_name;
        $prd_code = $request->product_code;
        $prd_price = $request->product_price;

          if($request->has('product_size'))
           $product_size =  implode(",",$request->product_size);
       else
           $product_size =  null; 

       $descp = $request->content;

        $saveData = [];

        
        $saveData['category_id'] = $catName;
        $saveData['product_name'] = $ProductName;
        $saveData['product_code'] = $prd_code;
        $saveData['product_descp'] = $descp;
        $saveData['product_size'] = $product_size;
        $saveData['product_price'] = $prd_price;

        $isUpdate = $this->product->where('id',$updateid)->update($saveData);

        if($request->hasFile('prd_image'))
        {         
            foreach($data['prd_image'] as $key1 => $image)
            {       
                $images_name = $image->getClientOriginalName();
                $image->move($this->product_path,$images_name);  
                $arr_data = [];
                $arr_data['product_id'] = $updateid;
                $arr_data['product_image'] = $images_name;
                foreach ($data['loop'] as $key2 => $val3) 
                {
                    if($key1==$key2)
                    {   
                      $arr_data['product_color'] = $val3; 
                  }
              }
              $this->prd_color->create($arr_data);            
          }
      }
      Session::flash('success','Data has been updated successfully.');
      return redirect('admin/product');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteproduct($id)
    {
        if($id!=null)
        {   
            $id = base64_decode($id);
            $isDelete = $this->product->where('id',$id)->delete();
            $img_color = $this->prd_color->where('product_id',$id)->delete();

            if($isDelete)
                Session::flash('success','Product has been deleted successfully');
            else
                Session::flash('error','Something is wrong');

            return redirect()->back();
        }    
        else
        {
            return redirect(config('app.project.login_slug'));
        }
    }

    public function deletproductimage($id)
    {
        if($id!=null)
        {   
            $id = base64_decode($id);
            $img_color = $this->prd_color->where('id',$id)->delete();

            if($img_color)
                Session::flash('success','Product Image has been deleted successfully');
            else
                Session::flash('error','Something is wrong');

            return redirect()->back();
        }    
        else
        {
            return redirect(config('app.project.login_slug'));
        }
    }

    public function productstatus($id)
    {
        if($id!=null)
        {   
            $id  = base64_decode($id);
            $data = $this->product->select('is_active')->where('id',$id)->first();

            if($data!=null)
            {
               $status = $data->is_active;
               if($status=='Y')
                $updArr = array('is_active'=>'N');
            else if($status=='N')
                $updArr = array('is_active'=>'Y');

            $isUpdate =   $this->product->where('id',$id)->update($updArr);

            Session::flash('success','Product status has been changed successfully');    
            return redirect()->back();
        }
    }
    else
    {
        return redirect(config('app.project.login_slug'));
    }
}
}
