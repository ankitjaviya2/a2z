<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryModel;
use Flash;
use Session;
use Storage;
use DB;
use DateTime;

class CategoryController extends Controller
{
    public function __construct(CategoryModel $categoryDtl)
    {
        $this->category = $categoryDtl;
        $this->category_path         = public_path() . config('app.project.category_path');
        $this->category_public_path = url('/') ."/public/".config('app.project.category_path');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $data =  $this->category->orderBy('id','DESC')->get();
     return view('admin.category.index',compact('data'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createcategory()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_name = $request->category_name;
        if($request->hasFile('category_image'))
        {
            $file_extension = strtolower($request->file('category_image')->getClientOriginalExtension());
            $file_name = $request->file('category_image')->getClientOriginalName();
            $image_size = getimagesize($request->file('category_image'));

            if(in_array($file_extension, ['png','jpeg','jpg']))
            {
                $file_name = time().uniqid(). '.'.$file_extension;
                $isUpload = $request->file('category_image')->move($this->category_path, $file_name);   
            }
        }
        else{
            $file_name = null;
        }

        $saveData = array('category_name'=>$category_name,'category_image'=>$file_name,
         'is_active'=>'Y');

        $this->category->create($saveData);
        Session::flash('success','Data has been saved successfully.');
        return redirect('admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if($id!=null)
        {
            $data =  $this->category->where('id',base64_decode($id))->first();
            return view('admin.category.edit',compact('data'));
        }
        else
        {
            return redirect(config('app.project.login_slug'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $updateid = $request->updateid;
       $category_name = $request->input('categoy_name');

       if($request->hasFile('cat_image'))
       {
        $file_extension = strtolower($request->file('cat_image')->getClientOriginalExtension());
        $file_name = $request->file('cat_image')->getClientOriginalName();
        $image_size = getimagesize($request->file('cat_image'));

        if(in_array($file_extension, ['png','jpeg','jpg']))
        {
            $file_name = time().uniqid(). '.'.$file_extension;
            $isUpload = $request->file('cat_image')->move($this->category_path, $file_name);   
        }
    }
    else{
        $file_name = $request->old_image;
    }

    $updateData = array('category_name'=>$category_name,'category_image'=>$file_name);
    $this->category->where('id',$updateid)->update($updateData);

    Session::flash('success','Data has been updated successfully.');
    return redirect('admin/category');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletecategory($id)
    {
        if($id!=null)
        {   
            $id = base64_decode($id);
            $isDelete = $this->category->where('id',$id)->delete();

            if($isDelete)
                Session::flash('success','Category has been deleted successfully');
            else
                Session::flash('error','Something is wrong');

            return redirect()->back();
        }    
        else
        {
            return redirect(config('app.project.login_slug'));
        }
    }

    public function categorystatus($id)
    {
        if($id!=null)
        {   
            $id  = base64_decode($id);
            $data = $this->category->select('is_active')->where('id',$id)->first();

            if($data!=null)
            {
             $status = $data->is_active;
             if($status=='Y')
                $updArr = array('is_active'=>'N');
            else if($status=='N')
                $updArr = array('is_active'=>'Y');

            $isUpdate =   $this->category->where('id',$id)->update($updArr);

            Session::flash('success','Category status has been changed successfully');    
            return redirect()->back();
        }
    }
    else
    {
        return redirect(config('app.project.login_slug'));
    }
}
}
