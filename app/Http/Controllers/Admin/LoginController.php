<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Str;
use Config;	

class LoginController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

	public function userlogin(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|max:255',
			'password' => 'required',
		]);

		if ($validator->fails())
		{
			Session::flash('error',Config::get('constants.Invalidcredentials'));
			return redirect()->back()->withInput();
		}

		$credentials = [
			'email'    => $request->input('email'),
			'password' => $request->input('password'),
		];

		$remember_me = $request->input('remember_me') ? true : false;

        if(!empty($remember_me)) 
        {   
            $username = $request->email;
            $password = $request->password;
            
            $expire = time() + 60 * 60 * 24 * 30;
            setcookie("member_login",$username,$expire);
            setcookie("member_password",$password,$expire);

        } else {
            if(isset($_COOKIE["member_login"]) && isset($_COOKIE["member_password"])) {
                setcookie ("member_login","");
                setcookie ("member_password","");
            }
        }

        if (Auth::attempt($credentials)) 
        {
         $user = User::where(["email" => $request->email])->first();	
         Auth::login($user, $remember_me);
         return redirect()->route('dashboard');
     }
     else
     {
      Session::flash('error',Config::get('constants.WrongUser'));
      return redirect()->back()->withInput();
  }	
}
public function logout()
{
   Auth::logout();
   return redirect('/');
}
}
