<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Route::get('login', 'Admin\LoginController@login')->name('login');


Route::group(['middleware' => 'web'], function () 
{
	Route::post('userlogin', 'Admin\LoginController@userlogin')->name('userlogin');
	Route::get('logout', 'Admin\LoginController@logout')->name('logout');

});

Route::group(['prefix' =>'admin', 'middleware' => ['admin']], function () 
{
	Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

	// category
	Route::any('/createcategory', 'Admin\CategoryController@createcategory');
	Route::any('/category', 'Admin\CategoryController@index');
	Route::any('/submitcategory', 'Admin\CategoryController@store');
	Route::any('/editcategory/{id}', 'Admin\CategoryController@edit');
	Route::any('/updatecategory', 'Admin\CategoryController@update');
	Route::any('/deletecategory/{id}', 'Admin\CategoryController@deletecategory');
	Route::any('/categorystatus/{id}', 'Admin\CategoryController@categorystatus');

	// prducts

	Route::any('/createproduct', 'Admin\ProductController@createproduct');
	Route::any('/product', 'Admin\ProductController@index');
	Route::any('/submitproduct', 'Admin\ProductController@store');
	Route::any('/editproduct/{id}', 'Admin\ProductController@edit');
	Route::any('/productview/{id}', 'Admin\ProductController@show');
	Route::any('/updateproduct', 'Admin\ProductController@update');
	Route::any('/deleteproduct/{id}', 'Admin\ProductController@deleteproduct');
	Route::any('/deletproductimage/{id}', 'Admin\ProductController@deletproductimage');
	Route::any('/productstatus/{id}', 'Admin\ProductController@productstatus');
	

});

